﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneSelect : MonoBehaviour {

    [SerializeField]
    Button m_scene1Btn;
    [SerializeField]
    Button m_scene2Btn;
    [SerializeField]
    Button m_quitBtn;

    private void OnEnable()
    {
        m_scene1Btn.onClick.AddListener(() => LoadScene(1));
        m_scene2Btn.onClick.AddListener(() => LoadScene(2));
        m_quitBtn.onClick.AddListener(QuitApp);
    }

    private void OnDisable()
    {
        m_scene1Btn.onClick.RemoveListener(() => LoadScene(1));
        m_scene2Btn.onClick.RemoveListener(() => LoadScene(2));
        m_quitBtn.onClick.RemoveListener(QuitApp);
    }

    void LoadScene(int index)
    {
        SceneManager.LoadScene(index);
    }

    void QuitApp()
    {
        Application.Quit();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
