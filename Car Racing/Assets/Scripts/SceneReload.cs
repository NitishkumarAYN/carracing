﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneReload : MonoBehaviour {

    [SerializeField]
    Button m_reloadBtn;
    [SerializeField]
    Button m_backBtn;

    private void OnEnable()
    {
        m_reloadBtn.onClick.AddListener(ReloadScene);
        m_backBtn.onClick.AddListener(GoToMainMenu);
    }

    private void OnDisable()
    {
        m_reloadBtn.onClick.RemoveListener(ReloadScene);
        m_backBtn.onClick.RemoveListener(GoToMainMenu);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void GoToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
